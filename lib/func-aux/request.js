const axios = require('axios')

module.exports = function request(axiosParams, data) {
  //console.log('PARAMETROS AXIOS: ', axiosParams)
  //console.log('CONSULTA: ', data)

  if (axiosParams === undefined || data === undefined)
    throw "a função request espera receber axiosParams e data"
  else {
    return axios({
      ...axiosParams,
      data: {
        query: data
      }
    }).then(res => res.data)
  }
}