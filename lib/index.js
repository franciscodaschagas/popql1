
const { filterData, filterParams, filterMethodsGql } = require('./func-aux/filters')
const { queryGenerate, mutationGenerate } = require('./func-aux/Generates')
const request = require('./func-aux/request')

const QUERY = true
const MUTATION = false

function exec(type, params, p) {
  return new Promise((resover, reject) => {
    try {
      const axiosParams = filterParams(params, p)
      const data = type ? queryGenerate(filterData(params)) : mutationGenerate(filterData(params))
      if (params.show) console.log(data)
      const methods = filterMethodsGql(params)

      request(axiosParams, data)
        .then(res => {
          if (res.data === null) reject(res.errors)
          else {
            let response = {}
            if (Array.isArray(methods) && methods.length > 1) {
              methods.forEach(el => response[el] = res.data[el])
            }
            else response[methods] = res.data[methods]
            resover(response)
          }
        }).catch(err => reject(err))
    } catch (err) {
      reject(err)
    }
  })

}

module.exports = function PopQl(p) {
  return {
    query(params) {
      return exec(QUERY, params, p)
    },
    mutation(params) {
      return exec(MUTATION, params, p)
    }
  }
}